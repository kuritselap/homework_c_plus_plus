
#include <iostream>

class Animal
{
public:
	virtual void Voice()
	{
		std::cout << "Animalz" << std::endl;
	}
};

class Dog : public Animal
{
public:
	void Voice() override
	{
		std::cout << "Woof!" << std::endl;
	}
};

class Cat : public Animal
{
public:
	void Voice() override
	{
		std::cout << "Meow!" << std::endl;
	}
};

class Fox : public Animal
{
public:
	void Voice() override
	{
		std::cout << "What does the fox say!" << std::endl;
	}
};


int main()
{
	Animal* AnArrayOfPointers[4] = { new Animal, new Dog, new Cat, new Fox };
	for (int i = 0; i < 4; i++)
	{
		AnArrayOfPointers[i]->Voice();
	}
	for (int i = 0; i < 4; i++)
	{
		delete AnArrayOfPointers[i];
		AnArrayOfPointers[i] = 0;
	}
	return 0;
}